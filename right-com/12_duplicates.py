# def numDuplicates(name, price, weight):
#     n = len(name)
#     items = []
#     duplicates_count = 0

#     if len(name) == len(set(name)):
#         return 0
#     if len(price) == len(set(price)):
#         return 0
#     if len(weight) == len(set(weight)):
#         return 0

#     for i in range(n):
#         h = []
#         h.append(name[i])
#         h.append(price[i])
#         h.append(weight[i])
#         items.append(h)

#     for i in range(0,n):
#         for j in range(i+1,n):
#             if items[i] == items[j]:
#                 duplicates_count += 1
#                 break
#                 # break so that once it finds one duplicate of 

#     return duplicates_count


def numDuplicates(name, price, weight):
    n = len(name)
    items = []
    duplicates_count = 0

    if len(name) == len(set(name)):
        return 0
    if len(price) == len(set(price)):
        return 0
    if len(weight) == len(set(weight)):
        return 0

    for i in range(n):
        unique_string_per_entry = str(name[i]) + "-" +str(price[i]) + "-" + str(weight[i])
        items.append(unique_string_per_entry)

    list_without_duplicates = len(list(set(items)))
    return n - list_without_duplicates


name = ['ball', 'bat', 'glove', 'glove', 'glove']
price = [2,3,1,2,1]
weight = [2,5,1,1,1]

print(numDuplicates(name, price, weight))



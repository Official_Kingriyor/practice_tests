
def barterMarket(comicBooks, coins, coinsNeeded, coinsOffered):
    coins_needed_for_available_comics = comicBooks * coinsNeeded
    while coins < coins_needed_for_available_comics:
        comicBooks -= 1
        coins += coinsOffered
        coins_needed_for_available_comics = comicBooks * coinsNeeded
        # still looking for a maths formulae to make while loop void thereby making algorithm           # faster, but i don't have enough time

    return comicBooks

comicBooks = 4000
coins = 6
coinsNeeded = 3
coinsOffered = 0

# comicBooks = 393
# coins = 896
# coinsNeeded = 787
# coinsOffered = 920

print(barterMarket(comicBooks, coins, coinsNeeded, coinsOffered))
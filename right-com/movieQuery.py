import requests

def getMovieTitles(substr):
    titles = []
    url = "https://jsonmock.hackerrank.com/api/movies/search/?Title="+ str(substr)
    res = requests.get(url)
    res = res.json()
    total_pages = int(res['total_pages'])

    for i in range(total_pages):
        url = "https://jsonmock.hackerrank.com/api/movies/search/?Title="+ str(substr) + "&page=" + str(i+1)
        res = requests.get(url)
        res = res.json()
        data_array = res['data']
        for movie in data_array:
            titles.append(movie['Title'])

    titles.sort()
    return titles


print(getMovieTitles("Spiderman"))




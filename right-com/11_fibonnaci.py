def fibonacci(n):
    series = [0,1]
    i = 0
    if n == 0:
        return []
    if n == 1:
        return [0]
    while len(series) < n:
        last_element = series[-1]
        second_to_last = series[-2]
        series.append(int(last_element) +int(second_to_last))
        i += 1

    return (series)

print(fibonacci(3))

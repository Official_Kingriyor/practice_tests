def restock(itemCount, target):
    # Write your code here
    purchased_units = 0
    for item in itemCount:
        # print(item)
        if purchased_units < target:
            purchased_units += int(item)
        else:
            break
    
    return abs(purchased_units - target)


# itemCount = [10, 20, 30, 40, 15]
# target = 80

# itemCount = [6,1,2,1]
# target = 100

itemCount = [1,2,3,2,1]
target = 4

print(restock(itemCount, target))


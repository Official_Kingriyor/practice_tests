# def candy(A):
#     num_of_candy_given = 0
#     A.sort()
#     set_hold = list(set(A))
#     print (set_hold)
#     for i in range(len(set_hold)):
#         print("i in loop => ",i)
#         current_val = set_hold[i]
#         num_c = A.count(current_val)
#         print ("number of occurences => ",num_c)
#         num_of_candy_given += ((i + 1) * num_c)
#         print ("Num of candys given => ", num_of_candy_given)
#         print("-----------")
#     return num_of_candy_given
# [1,5,2,1]


# def highest_neighbour(A, i):
#     index_of_highest_neighbour_i_am_bigger_than = -1
#     if (i == 0):
#         if (A[0] > A[1]):
#             print("M1")
#             return 1
#     elif (i == (len(A) - 1)):
#         print("M2u")
#         if (A[-1] => A[-2]):
#             print("M2")
#             return (len(A) - 2)
#     else:
#         if A[i] >= A[i-1]:
#             if A[i-1] >= A[i+1]:
#                 return i-1
#         if A[i] >= A[i+1]:
#             if A[i+1] >= A[i-1]:
#                 return i+1
#     return -1


# def candy(A):
#     candys_given = [1] *len(A)
#     for i in range(len(A)):
#         print ("Current index => ",i)
#         index_of_highest_neighbour_i_am_bigger_than = highest_neighbour(A, i)
#         print ("index_of_highest_neighbour_i_am_bigger_than => ",index_of_highest_neighbour_i_am_bigger_than)
#         if index_of_highest_neighbour_i_am_bigger_than != -1:
#             candys_given[i] = candys_given[index_of_highest_neighbour_i_am_bigger_than]  + 1
#         print (candys_given)

            
    # print("--------------")
    # return sum(candys_given)

def candy(array):
    res = 1
    prev_candy = 1
    descent_len = 1
    descent_top = 1
    for i, cur in enumerate(array[1:]):
        prev = array[i]
        if cur < prev:
            descent_len += 1
            res += descent_len - 1
            if descent_top < descent_len:
                descent_top += 1
                res += 1
            prev_candy = 1
        else:
            if cur == prev:
                prev_candy = 1
            else:
                prev_candy += 1
            res += prev_candy
            descent_len = 1
            descent_top = prev_candy
    return res


A = [1,5,2,1]
# A = [1,2,2]
print (candy(A))
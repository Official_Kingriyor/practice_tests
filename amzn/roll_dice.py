
def rotate(cur, dest):
    if cur==dest:
        return 0
    if cur+dest!=7:
        return 1
    return 2
    
def solution(A):
    moves = [None]*(len(A)+1)
    moves[0] = [0,0,0,0,0,0]
    for i in range(1, len(A)+1):
        moves[i] = [None]*6
        for j in range(6):
            moves[i][j] = rotate(A[i-1], j+1)+moves[i-1][j]
    return min(moves[-1])


print(solution([1, 6, 2, 3]))
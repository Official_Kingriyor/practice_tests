def solution(truckSpace, packagesSpace):
    space_after_reservation = truckSpace - 30
    space_by_unique_2 = []
    mapping = {}

    for i in range(len(packagesSpace) - 1):
        for j in range(i+1,len(packagesSpace)):
            space_sum = packagesSpace[i] + packagesSpace[j]
            space_by_unique_2.append(space_sum)
            mapping[space_sum] = [i,j]

    space_by_unique_2.sort()
    found1 = -1

    for i in range(len(space_by_unique_2)):
        if space_by_unique_2[i] > space_after_reservation:
            found1 = space_by_unique_2[i-1]
            break

    return mapping.get(found1)



truckSpace = 90
packagesSpace = [1, 10, 25, 35, 60]

print(solution(truckSpace, packagesSpace))
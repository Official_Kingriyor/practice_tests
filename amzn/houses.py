def cellCompete(states, days):
    # WRITE YOUR CODE HERE
    # create array of same length and populate with 0s
    new_state = [1] * len(states)
    
    
    for x in range(days):
        for i in range(len(states)):
            if i == 0:
                if states[i+1] == 0:
                    new_state[i] = 0
            if i == len(states) - 1:
                if states[i-1] == 0:
                    new_state[i] = 0
            
            if i != 0 and i != len(states) - 1 and states[i-1] == states[i+1]:
                new_state[i] = 0
                
            if i != 0 and i != len(states) - 1 and states[i-1] != states[i+1]:
                new_state[i] = 1
        
        states = new_state.copy()

    return new_state

states = [1,0,0,0,0,1]
days = 1

print (cellCompete(states, days))
# Actual test id = 23280666314186

def threeProductSuggestions(numProducts, repository, customerQuery):
    # WRITE YOUR CODE HERE
    
    query_lenght = len(customerQuery)
    if query_lenght == 1:
        return []
    final_result = [0]*query_lenght
    repository.sort()

    for x in range(query_lenght):
        result = []
        for product in repository:
            if customerQuery[:x+1].lower() in product.lower():
                result.append(product)

        if len(result) > 3:
            result = result[:3]
        
        final_result[x] = result

    return final_result[1:]

numProducts = 5
repository = ["mobile", "mouse", "moneypot", "monitor", "mousepad"]
customerQuery = "mouse"

print (threeProductSuggestions(numProducts, repository, customerQuery))


# def threeProductSuggestions(numProducts, repository, customerQuery):
#     # WRITE YOUR CODE HERE
    
#     query_lenght = len(customerQuery)
#     final_result = [0]*query_lenght
#     repository.sort()

#     for x in range(query_lenght):
#         result = []
#         for product in repository:
#             if customerQuery[:x+1].lower() in product.lower():
#                 result.append(product)

#         if len(result) > 3:
#             result = result[:3]
        
#         final_result[x] = result

#     return final_result
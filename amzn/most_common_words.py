import re
from collections import Counter

def solution (text, wordsToExclude):
    # force string to lower case
    text = text.lower()
    # remove all special characters and replace with a space
    text = re.sub('[^a-zA-Z0-9 \n\.]', ' ', text)
    # remove fullstop
    text = text.replace(".","")
    # split remaining text into words in a list
    words=text.split(" ")
    # remove all "" in the list
    words = [x for x in words if x]
    # convert all words in wordsToExclude to lowercase
    wordsToExclude = [x.lower() for x in wordsToExclude if x]
    # exclude all words in wordsToExclude that exists in the main word list
    words = [x for x in words if x not in wordsToExclude]

    result = set()
    for x in words:
        # if a word has an occurence greater than 1, add to result set
        if words.count(x) > 1:
            result.add(x)

    return list(result)


def test_solution2(text, wordsToExclude):
    
    # force string to lower case
    text = text.lower()
    # remove all special characters and replace with a space
    text = re.sub('[^a-zA-Z0-9 \n\.]', ' ', text)
    # remove fullstop
    text = text.replace(".","")

    word_list = text.split()

    # remove all "" in the list
    word_list = [x for x in word_list if x]
    # convert all words in wordsToExclude to lowercase
    wordsToExclude = [x.lower() for x in wordsToExclude if x]
    # exclude all words in wordsToExclude that exists in the main word list
    word_list = [x for x in word_list if x not in wordsToExclude]

    # Get the count of each word.
    word_count = Counter(word_list)

    # Use most_common() method from Counter subclass
    return word_count #.most_common(3)


text = "Jack and Jill went to the market, to buy bread and cheese. Cheese is Jack’s and Jill’s favorite food."
wordsToExclude = ["and", "he", "the", "to", "is", "Jack", "Jill" ]
print(solution (text, wordsToExclude))
# print(test_solution2(text, wordsToExclude))
import math
import json

def euclidean_distance(p,q):
    dimension = len(p)
    hold = 0
    for i in range(dimension):
        hold += (p[i] - q[i]) **2
    distance = math.sqrt(hold)
    return distance

def solution(post_offices, k):
    distances = []
    mapping = {}
    for post_office in post_offices:
        e_dist = euclidean_distance([0,0], post_office)
        distances.append(e_dist)
        mapping[e_dist] = post_office

    result = []
    distances.sort()
    winning3 = distances[:3]
    mapping = json.dumps(mapping)
    mapping = json.loads(mapping)
    for n in winning3:
        n = str(n)
        result.append(mapping.get(n))
    return result


post_offices = [[-16, 5], [-1, 2], [4, 3], [10, -2], [0, 3], [-5, -9]]
k = 3

print(solution(post_offices, k))
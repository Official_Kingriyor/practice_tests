import copy 

def minimumDays(rows, columns, grid):
    move_count = 0
    row_size = rows
    column_size = columns
    # deepcopy to create a fresh independent copy
    new_mat = copy.deepcopy(grid)

    while does_zero_exist(grid) == True:
        
        move_count += 1
        for i in range(row_size):
            for j in range(column_size):
                if grid[i][j] == 1:
                    adjacent_points = index_search([i,j], row_size, column_size)
                    for p in adjacent_points:
                        if p!= -1:
                            new_mat[p[0]][p[1]] = 1
                
        grid = copy.deepcopy(new_mat)
        print(grid)

        
    return move_count

def index_search(point, row_size, column_size):
    # result = [up, left, down, right]
    # This function gives you all the adjacent points 
    i = point[0]
    j = point[1]
    up = [0,0]
    down = [0,0]
    right = [0,0]
    left = [0,0]

    if i == 0:
        up = -1
    else:
        up[0] = i - 1
        up[1] = j

    if i>= row_size - 1:
        down = -1
    else:
        down[0] = i + 1
        down[1] = j

    if j == 0:
        left = -1
    else:
        left[0] = i
        left[1] = j - 1

    if j >= column_size - 1:
        right = -1
    else:
        right[0] = i
        right[1] = j + 1

    return [up, left, down, right]

def does_zero_exist(mat):
    # This function simply lets me know if any zero is left in the matrix
    for i in range(len(mat)):
        for j in range(len(mat[0])):
            if mat[i][j] == 0:
                return True
    return False

rows = 5
columns = 5
grid = [
    [1, 0, 0, 0, 0],
    [0, 1, 0, 0, 0],
    [0, 0, 1, 0, 0],
    [0, 0, 0, 1, 0],
    [0, 0, 0, 0, 1]
    ]   

print (minimumDays(rows, columns, grid))


import copy
def solution(mat):
    move_count = 0
    row_size = len(mat)
    column_size = len(mat[0])
    new_mat = copy.deepcopy(mat)

    while does_zero_exist(mat) == True:
        move_count += 1
        for i in range(len(mat)):
            for j in range(len(mat[0])):
                if mat[i][j] == 1:
                    adjacent_points = index_search([i,j], row_size, column_size)
                    for p in adjacent_points:
                        if p!= -1:
                            new_mat[p[0]][p[1]] = 1
                
        mat = copy.deepcopy(new_mat)

        
    return move_count

def index_search(point, row_size, column_size):
    # result = [up, left, down, right]
    i = point[0]
    j = point[1]
    up = [0,0]
    down = [0,0]
    right = [0,0]
    left = [0,0]

    if i == 0:
        up = -1
    else:
        up[0] = i - 1
        up[1] = j

    if i>= row_size - 1:
        down = -1
    else:
        down[0] = i + 1
        down[1] = j

    if j == 0:
        left = -1
    else:
        left[0] = i
        left[1] = j - 1

    if j >= column_size - 1:
        right = -1
    else:
        right[0] = i
        right[1] = j + 1

    return [up, left, down, right]

def does_zero_exist(mat):
    for i in range(len(mat)):
        for j in range(len(mat[0])):
            if mat[i][j] == 0:
                return True
    return False




test_old = [['Mon', '18', '20', '22', '17'],
        ['Tue', '11', '18', '21', '18'],
        ['Wed', '15', '21', '20', '19'],
        ['Thu', '11', '20', '22', '21'],
        ['Fri', '18', '17', '23', '22'],
        ['Sat', '12', '22', '20', '18'],
        ['Sun', '13', '15', '19', '16']]

test = [ [0, 1, 1, 0, 1],
        [0, 1, 0, 1, 0],
        [0, 0, 0, 0, 1],
        [0, 1, 0, 0, 0]
    ]

# r1 = [  [1, 1, 1, 0, 1], 
#         [0, 1, 0, 1, 0], 
#         [0, 0, 0, 0, 1], 
#         [0, 1, 0, 0, 0] ]

#         [[0, 1, 1, 0, 1],
#          [0, 1, 0, 1, 0], 
#          [0, 0, 0, 0, 1], 
#          [0, 1, 0, 0, 0]]

#         [[1, 1, 1, 0, 1], 
#         [0, 1, 0, 1, 0], 
#         [0, 0, 0, 0, 1], 
#         [0, 1, 0, 0, 0]]

test = [ [1, 0, 0],
        [0, 0, 0],
        [0, 0, 0]
    ]

# print (does_zero_exist(test))

# print(test[6][2])

# row_size = len(test)
# column_size = len(test[0])
# # print(column_size)
# point = [6,4] #20 wed
# print(index_search(point, row_size, column_size))

# print (test[0][2])
# print (test[2][2])
# print (test[3][3])
# print (test[2][4])


print (solution(test))




def solution(movie_durations, flight_time):
    if len(movie_durations) == 0 or len(movie_durations) == 1:
        return -1
    if len(movie_durations) == 2:
        return movie_durations
        
    allowed_time = flight_time - 30
    possible_combinations = []
    info_hold = {}
    for i in range(len(movie_durations)-1):
        for j in range(i+1 , len(movie_durations)):
            duration = int(movie_durations[i] + movie_durations[j])
            conbination = [movie_durations[i] , movie_durations[j]]
            info_hold[duration] = conbination
            possible_combinations.append(movie_durations[i] + movie_durations[j])

    possible_combinations.sort()

    found_index = -1
    
    for n in range(len(possible_combinations)):
        if possible_combinations[n] > allowed_time:
            found_index = n-1
            break

    if found_index == -1:
        return -1

    found_time_conbination = possible_combinations[found_index]
    return info_hold[found_time_conbination]




movie_durations =  [90, 85, 75, 60, 120, 150, 125]
flight_time = 250
print(solution(movie_durations, flight_time))
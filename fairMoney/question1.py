import requests

def topArticles(username, limit):
    pageNumber = 1
    # Write your code here
    titles = []
    num_comments_counts = []
    url = "https://jsonmock.hackerrank.com/api/articles?author="+ str(username) + "&page=" + str(pageNumber)
    res = requests.get(url)
    res = res.json()
    data_array = res['data']
    Article_count = 0
    current_pageSize = len(data_array)
    counter = 1
    for data in data_array:
        if data['title'] == None and data['story_title']== None:
            break 
        titles.append(data)
        num_comments_counts.append(data['num_comments'] if data['num_comments'] != None else 0)


    stop = 0

    while stop!=1:
        url = "https://jsonmock.hackerrank.com/api/articles?author="+ str(username) + "&page=" + str(pageNumber + 1)
        res = requests.get(url)
        res = res.json()
        data_array = res['data']
        next_pageSize = len(data_array)
        if next_pageSize !=0:
            for data in data_array:
                if data['title'] == None and data['story_title']== None:
                    break 
                titles.append(data)
                num_comments_counts.append(int(data['num_comments']) if data['num_comments'] != None else 0)
        else:
            stop = 1


    num_comments_counts = sorted(num_comments_counts, reverse=True)
    num_comments_counts = num_comments_counts[:limit]

    result = []

    for num_comments in num_comments_counts:
        if num_comments == 0:
            num_comments = None
            print (num_comments)
        for title in titles:
            if title['num_comments'] == num_comments:
                if title['title'] == None:
                    result.append(title['story_title'])
                else:
                    result.append(title['title'])

    return result

# te = topArticles("olalonde", 2)
# print(te)








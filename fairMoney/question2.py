
import math

def connectedSum(n, edges):
    # Write your code here
    result = 0
    global_i_joined = []
    connectedCounts = []
    head_nodes = []
    for i in range(1,n+1):
        i_count = 1
        i_joined = []
        
        for x in edges:
            head_nodes.append(i)
            if x[1] in head_nodes:
                head_nodes.remove(x[1])
            
            if x[0] in i_joined:
                i_count += 1
                i_joined.append(x[1])
                global_i_joined.append(x[0])
            elif x[0] == i:
                i_count += 1
                i_joined.append(x[1])

        connectedCounts.append(math.ceil(i_count**0.5))

    
    global_i_joined = list(set(global_i_joined))
    
    for i in range(n):
        if i+1 in head_nodes:
            result += connectedCounts[i]
             
    return result

n = 10
edges = [[1,2],[1,3],[2,4],[3,5],[7,8]]

print(connectedSum(n, edges))  